(function ($) {
  jQuery(document).ready(function($) {

    var bootstrapMenu = $("ul.bootstrap-menu");

    bootstrapMenu.find("li.dropdown > ul.dropdown-menu").hover(function(){
      $(this).parents("li.dropdown").find("a").addClass( "hover" );
    },
    function(){
      $(this).parents("li.dropdown").find("a").removeClass( "hover" );
    })


  });
}(jQuery));
